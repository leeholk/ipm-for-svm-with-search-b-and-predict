#!/usr/bin/env python
# coding: utf-8

# In[200]:

import newton_sketch
import os
os.chdir('C:/Users/lee hao/Documents/Operation project/IPM-SVM/IPM-SVM')
from scipy.optimize  import linprog
from IPM import IPM
from sklearn.datasets import load_svmlight_file
import os
from scipy.linalg import cholesky
import numpy as np
from numpy.linalg import solve
from scipy.sparse import coo_matrix, block_diag, eye, dia_matrix, csc_matrix, hstack, vstack
#from scipy.linalg import ldl
from scipy.sparse.linalg import spsolve
from numpy.linalg import norm
from numpy.linalg import inv as dense_inv
from scipy.sparse.linalg import inv


class SVM(IPM):
    def __init__(self, tao):
        # os.chdir('C:/Users/Kirik/PycharmProjects/IPM-SVM')
        os.chdir('C:/Users/lee hao/Documents/Operation project/IPM-SVM/IPM-SVM')
        self.tao = tao
        self.X={}
        self.Y={}


        pass
    def class_preprocess(self):
        n=self.Y[-1].shape[0]
        self.classtype=[]
        for i in range(n):
            if self.Y[-1][i] not in self.classtype:
                self.classtype+=[self.Y[-1][i]]
        self.classnum=len(self.classtype)
        for i in range(self.classnum):
            self.Y[i]=self.Y[-1]
            for j in range(n):
                if self.Y[-1][j]!=self.classtype[i]:
                    self.Y[i][j]=-1
                else:
                    self.Y[i][j]=1
        for i in range(self.classnum):
            self.X[i]=self.X[0]
        return self.classnum
    
    def init_point(self,A_w,A_z,Q_w,Q_z,c_w,c_z,b,u):
        y_ = inv(A_z.dot(A_z.T)).dot(2*b-A_z.dot(u))
        z = (A_z.T.dot(y_)+u)/2
        y = inv(A_z.dot(A_z.T)).dot(A_z).dot(c_z)
        s = 1/2*(A_z.T.dot(y)-c_z)
        w = np.zeros(A_w.shape[1])
        v = -s
        v = np.clip(v,1e-5,None)
        s = np.clip(s,1e-5,None)
        z = np.clip(z,1e-5,u-1e-5)
        return  w, z, y, s, v
    def load_data(self, filename):

        data = load_svmlight_file(filename)
        self.X = data[0]
        self.Y = data[1]
        self.Y[self.Y == 0] = -1
        print('data loaded')
        return

    def data_preprocess(self):

        self.X = self.X.T
        print(self.X.shape)
        self.n = self.X.shape[1]
        self.m = self.X.shape[0]
#        assert self.m<self.n
        '''
        self.Q_w = block_diag(np.ones(self.m))#.toarray()
        self.A_w = csc_matrix(np.eye(self.m+1,self.m))#.toarray()
        self.A_z = csc_matrix(np.eye(self.m+1,self.n))#.toarray()
        #self.Q_z = block_diag(np.ones(self.n))
        self.Q_z = csc_matrix(np.zeros((self.n,self.n)))#.toarray()
        self.c_w = np.zeros(self.m)
        self.c_z = np.zeros(self.n)
        self.u = 10*np.ones(self.n)
        self.b = np.append(-5*np.ones(self.m),5)
        print(self.b)
        '''
       
        for i in range(self.n):
            self.X[:, i] = self.X[:, i] * self.Y[i]
        self.XY = self.X
        # del self.X
        print('XY computed')
        Q_w = np.ones(self.m)
        self.Q_w = block_diag(Q_w)
        print('Q_w prepared')
        assert self.Q_w.shape == (self.m, self.m)
        del Q_w
        Q_z = np.ones(self.n)/self.tao
        self.Q_z = block_diag(Q_z)
       # self.Q_z = csc_matrix((self.n, self.n))
        print('Q_z prepared')
        # 需要改成稀疏矩阵形式存储
        self.A_w = vstack((block_diag(np.ones(self.m)), csc_matrix((1, self.m))))
        print(self.XY.shape)
        print(self.Y.shape)
        self.A_z = vstack((-self.XY, self.Y.T))
        assert self.A_w.shape == (self.m + 1, self.m)
        assert self.A_z.shape == (self.m + 1, self.n)
        print('A_w,A_z prepared')

        self.c_z = np.ones(self.n)
        self.c_w = np.zeros(self.m)
        print('c_w,c_z prepared')

        self.b = np.zeros(self.m + 1)
        print('b prepared')

        self.u = self.tao * np.ones(self.n)
        print('u prepared')
        
        return self.A_w, self.A_z, self.Q_w, self.Q_z, self.c_w, self.c_z, self.b, self.u
    def sketch(self, data):
        return newton_sketch.fjlt(data,data.shape[1],0.9)
    def fit(self,
            maxk=130, eta=0.99995, eps_r_b=1e-3, K=3,
            eps_r_cw=1e-3, eps_r_cz=1e-3, eps_mu=1e-3, delta=0.1, beta_min=0.1, beta_max=10, gamma=0.1,
            ):
        A_w, A_z, Q_w, Q_z, c_w, c_z, b, u = self.data_preprocess()
        w, z, y, s, v = self.init_point(A_w, A_z, Q_w, Q_z, c_w, c_z, b, u)
       # print(A_w.shape)
        k = 0
        assert w.shape == (self.m,)
        assert z.shape == (self.n,)
        assert s.shape == (self.n,)
        assert v.shape == (self.n,)
        assert y.shape == (self.m + 1,)
        #print(A_w.shape,(self.m + 1, self.m))
        #print(A_w.shape == (self.m + 1, self.m))
        assert A_w.shape == (self.m + 1, self.m)
        assert A_z.shape == (self.m + 1, self.n)
        assert Q_w.shape == (self.m, self.m)
        assert Q_z.shape == (self.n, self.n)
        assert c_w.shape == (self.m,)
        assert c_z.shape == (self.n,)
        assert b.shape == (self.m + 1,)
        assert u.shape == (self.n,)
        # 输入为 rank 1 array
        Theta_inv = block_diag(s / z + v / (u - z))
#Theta = block_diag(z/s+(u-z)/v)
        r_b = -(A_w.dot(w) + A_z.dot(z) - b)
#print(A_z.shape)
#print('r_b',r_b)
        r_cw = -(-Q_w.dot(w) + A_w.T.dot(y) - c_w)
#print('r_cw',r_cw)
        r_sz = -z*s
        r_vz = -(u-z)*v
        r_cz = -A_z.T.dot(y) +c_z +Q_z.dot(z)-s+v
        r_cz_hat = r_cz-r_sz/z+r_vz/(u-z)#Theta_inv.dot(z))
#print('r_cz',r_cz)
# 2n((z0)Ts0+(u−z0)Tv0)#.toarray()
#self.Theta_inv = Theta_inv
#print('Theta_inv',Theta_inv)
        mu = (s.dot(z) + (u - z).dot(v)) / (2 * self.n)
#r_b_hat = r_b + A_z.dot(Q_z + Theta_inv).dot(r_cz)
#print('r_b_hat',r_b_hat)
# print(A_w.dot(inv(Q_w)).dot(r_cw).shape)
        r_b_hat = r_b+A_w.dot(inv(Q_w)).dot(r_cw)+A_z.dot(inv(Q_z+Theta_inv)).dot(r_cz_hat)
        while k <= maxk and (norm(r_b) / (1 + norm(self.b)) >= eps_r_b or norm(r_cw) / (1 + norm(c_w)) >= eps_r_cw \
                             or norm(r_cz) / (1 + norm(c_z)) >= eps_r_cz or mu >= eps_mu):
            print(k)
            M_w = A_w.dot(inv(Q_w)).dot(A_w.T)
            # print(M_w)
            M_z = A_z.dot(inv((Q_z + Theta_inv))).dot(A_z.T)
            M = (M_w + M_z).toarray()
            # Factorization
            # print(M)

            L = cholesky(M)
            # print(L)
            # d_y_aff = dense_inv(L).dot(dense_inv(L.T).dot(r_b_hat_hat.T.ravel()))
            d_y_aff = dense_inv(L).dot(dense_inv(L.T).dot(r_b_hat.T.ravel()))
            d_w_aff = inv(Q_w).dot(A_w.T.dot(d_y_aff) - r_cw)
            d_z_aff = inv(Q_z + Theta_inv).dot(A_z.T.dot(d_y_aff) - r_cz_hat)
            print(inv(Q_z + Theta_inv))
            d_s_aff = -(s + s / z * d_z_aff)
            d_v_aff = -v + v / (u - z) * d_z_aff
            print('test affine')
            print('r_b', norm(A_w.dot(w + d_w_aff) + A_z.dot(z + d_z_aff) - b))
            print('r_cw', norm(-Q_w.dot(d_w_aff) + A_w.T.dot(d_y_aff) - r_cw))
            print('r_cz', norm(-(Q_z + Theta_inv).dot(d_z_aff) + A_z.T.dot(d_y_aff) - r_cz_hat))
            print('end')
            print('\n')
            # print('d_w_aff',d_w_aff)
            # print('d_z_aff',d_z_aff)
            # print('d_s_aff',d_s_aff)
            # print('d_v_aff',d_v_aff)
            # print('d_y_aff',d_y_aff)
            alpha_aff_p = 1
            alpha_aff_d = 1
            idx_z = np.where(d_z_aff < 0)[0]
            idx_uz = np.where(d_z_aff > 0)[0]
            idx_s = np.where(d_s_aff < 0)[0]
            idx_v = np.where(d_v_aff < 0)[0]
            if idx_z.size != 0:
                alpha_aff_p = min(alpha_aff_p, np.min(-z[idx_z] / d_z_aff[idx_z]))
            if idx_uz.size != 0:
                alpha_aff_p = min(alpha_aff_p, np.min((u - z)[idx_uz] / d_z_aff[idx_uz]))
            if idx_s.size != 0:
                alpha_aff_d = min(alpha_aff_d, np.min(-s[idx_s] / d_s_aff[idx_s]))
            if idx_v.size != 0:
                alpha_aff_d = min(alpha_aff_d, np.min(-v[idx_v] / d_v_aff[idx_v]))
            print('affine step length')
            print('alpha_aff_p', alpha_aff_p)
            print('alpha_aff_d', alpha_aff_d)
            print('\n')
            z_aff = z + d_z_aff * alpha_aff_p
            s_aff = s + d_s_aff * alpha_aff_d
            v_aff = v + d_v_aff * alpha_aff_d
            y_aff = y + d_y_aff * alpha_aff_d
            w_aff = w + d_w_aff * alpha_aff_p
            assert (z_aff >= -1e-8).all(), '%d' % z_aff[np.where(z_aff <= 0)]
            assert (s_aff >= -1e-8).all(), '%d' % s_aff[np.where(s_aff <= 0)]
            assert (v_aff >= -1e8).all(), '%d' % v_aff[np.where(v_aff <= 0)]
            assert (u - z_aff >= -1e-6).all(), '%d' % (u - z_aff)[np.where((u - z_aff) <= 0)]
            # print('z_aff',z_aff)
            # print('s_aff',s_aff)
            # print('w_aff',w_aff)
            # print('v_aff',v_aff)
            # print('y_aff',y_aff)
            mu_aff = ((s_aff).dot(z_aff) + (u - z_aff).dot(v_aff)) / (2 * self.n)
            print('affine gap', mu_aff)
            print('\n')
            # Determine centering parameter
            sigma = (mu_aff / mu) ** 3
            mu_t = sigma * mu
            e = np.ones(z.shape[0])
            r_sz = -z * s - d_z_aff * d_s_aff + mu_t * e
            r_vz = -v * (u - z) + d_z_aff * d_v_aff + mu_t * np.ones(z.shape[0])
            r_cz = -A_z.T.dot(y) + c_z + Q_z.dot(z) - s + v
            r_cz_hat = r_cz - r_sz / z + r_vz / (u - z)  # Theta_inv.dot(z))
            r_b_hat = r_b + A_w.dot(inv(Q_w)).dot(r_cw) + A_z.dot(inv(Q_z + Theta_inv)).dot(r_cz_hat)
            # Solve system
            d_y_pre_corr = dense_inv(L).dot(dense_inv(L.T).dot(r_b_hat.ravel()))
            d_w_pre_corr = inv(Q_w).dot(A_w.T.dot(d_y_pre_corr) - r_cw)
            d_z_pre_corr = inv(Q_z + Theta_inv).dot(A_z.T.dot(d_y_pre_corr) - r_cz_hat)
            d_s_pre_corr = r_sz / z - s * d_z_pre_corr / z
            d_v_pre_corr = r_vz / (u - z) + v * d_z_pre_corr / (u - z)
            print('test correct')
            print('r_b', norm(A_w.dot(w + d_w_pre_corr) + A_z.dot(z + d_z_pre_corr) - b))
            print('r_cw', norm(-Q_w.dot(d_w_pre_corr) + A_w.T.dot(d_y_pre_corr) - r_cw))
            print('r_cz', norm(-(Q_z + Theta_inv).dot(d_z_pre_corr) + A_z.T.dot(d_y_pre_corr) - r_cz_hat))
            print('end')
            print('\n')
            # Compute predictor step alpha_p
            alpha_p = 1
            alpha_d = 1
            idx_z = np.where(d_z_pre_corr < 0)[0]
            idx_uz = np.where(d_z_pre_corr > 0)[0]
            idx_s = np.where(d_s_pre_corr < 0)[0]
            idx_v = np.where(d_v_pre_corr < 0)[0]
            if idx_z.size != 0:
                alpha_p = min(alpha_p, np.min(-z[idx_z] / d_z_pre_corr[idx_z]))
            if idx_uz.size != 0:
                alpha_p = min(alpha_p, np.min((u - z)[idx_uz] / d_z_pre_corr[idx_uz]))
            if idx_s.size != 0:
                alpha_d = min(alpha_d, np.min(-s[idx_s] / d_s_pre_corr[idx_s]))
            if idx_v.size != 0:
                alpha_d = min(alpha_d, np.min(-v[idx_v] / d_v_pre_corr[idx_v]))
            print()
            print('correct step length')
            print('alpha_p', alpha_p)
            print('alpha_d', alpha_d)
            print('\n')
            d_w = d_w_pre_corr
            d_z = d_z_pre_corr
            d_s = d_s_pre_corr
            d_v = d_v_pre_corr
            d_y = d_y_pre_corr

            w += d_w * eta * alpha_p
            # _hat
            z += d_z * eta * alpha_p
            # _hat
            s += d_s * eta * alpha_d
            # _hat
            v += d_v * eta * alpha_d
            # _hat
            y += d_y * eta * alpha_d
            # print('z',z)
            # print('s',s)
            # print('w',w)
            # print('v',v)
            # print('y',y)

            # print(A_z.shape)
            # print('r_b',r_b)

            # print('r_cw',r_cw)
            print('###############')
            print('result')
            r_b = -(A_w.dot(w) + A_z.dot(z) - b)
            print('r_b', norm(r_b) / (1 + norm(self.b)))
            # print('d_w',d_w)
            # print('r_b',r_b)
            r_cw = -(-Q_w.dot(w) + A_w.T.dot(y) - c_w)
            print('r_cw', norm(r_cw) / (1 + norm(c_w)))
            r_sz = -z * s
            r_vz = -(u - z) * v
            r_cz = -A_z.T.dot(y) + c_z + Q_z.dot(z) - s + v
            r_cz_hat = r_cz - r_sz / z + r_vz / (u - z)  # Theta_inv.dot(z))
            print('r_cz', norm(r_cz) / (1 + norm(r_cz)))
            # print('r_cz',r_cz)
            print('mu', mu)
            print('\n')
            # 2n((z0)Ts0+(u−z0)Tv0)
            Theta_inv = block_diag(s / z + v / (u - z))  # .toarray()
            # print('Theta_inv',Theta_inv)
            mu = (s.dot(z) + (u - z).dot(v)) / (2 * self.n)
            # print('r_b_hat',r_b_hat)
            r_b_hat = r_b + A_w.dot(inv(Q_w)).dot(r_cw) + A_z.dot(inv(Q_z + Theta_inv)).dot(r_cz_hat)
            # print('r_b_hat_hat',r_b_hat_hat)
            k += 1

        self.w=w
      #  print (self.w*self.X)
        return w,z,y,s,v
    def search_b(self,a,b):
        s_a=0
        s_b=0
       # print(a,b)
        if abs(a-b)<=1:
            return (self.wx_sorted[a]+self.wx_sorted[b])/2
        for i in range(self.n):
              if self.wx[i]>self.wx_sorted[a] and self.Y[i]<0:
                   s_a+=self.wx[i]-self.wx_sorted[a]
              if self.wx[i]<self.wx_sorted[a] and self.Y[i]>0:
                  s_a+=self.wx_sorted[a]-self.wx[i]
              if self.wx[i]>self.wx_sorted[b] and self.Y[i]<0:
                   s_b+=self.wx[i]-self.wx_sorted[b]
              if self.wx[i]<self.wx_sorted[b] and self.Y[i]>0:
                  s_b+=self.wx_sorted[b]-self.wx[i]
       # print(s_a,s_b)
        if s_a<s_b:
            return self.search_b(a,int((a+b)/2))
        else:
            return self.search_b(int((a+b)/2),b)

    def predict(self,filename):
        self.wx=self.w*self.X
        self.wx_sorted=sorted(self.wx)
        b=self.search_b(0,self.n-1)
       # print (b)
        data = load_svmlight_file(filename)
        X_test=data[0]
        Y_test=data[1]
        n=X_test.shape[0]
        Y_pred=np.empty(n)
        acc=0
     #   print (X_test[1,:].shape)
       # for i in range(self.n):

        for i in range(n):
            Y_pred[i]=2*(self.wx[i]>b)-1
            if Y_pred[i]==Y_test[i]:
                acc+=1
       # print(Y_pred)
        print(acc/n)
        return Y_pred


# In[201]:


c = SVM(tao=0.2)
c.load_data('svm.txt')
y=c.Y
#w = np.ones(c.m)
#z = 2*np.ones(c.n)
#s = np.ones(c.n)
#v = np.ones(c.n)
#y = np.ones(c.m+1)



# In[202]:
import datetime

starttime = datetime.datetime.now()

w,z,y,s,v =c.fit()


endtime = datetime.datetime.now()

print (endtime - starttime)
y_pred=c.predict("svm.txt")
#print(c.predict("svm_1.txt"))



# In[ ]:




