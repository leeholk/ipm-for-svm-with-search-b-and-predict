from abc import abstractmethod, ABCMeta

class IPM(metaclass=ABCMeta):
    @abstractmethod
    def __init__(self):
         pass

    @abstractmethod
    def load_data(self, filename):
        return

    @abstractmethod
    def sketch(self, data):
        return

    @abstractmethod
    def fit(self, data):
        return

    @abstractmethod
    def data_preprocess(self):
        return

    @abstractmethod
    def search_b(self):
        return
    @abstractmethod
    def class_preprocess(self):
        return
    def predict(self,filename):
        return